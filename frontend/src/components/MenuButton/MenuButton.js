import React, {useState} from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisV , faTimes } from '@fortawesome/free-solid-svg-icons'

import './MenuButton.css'

const MenuButton = (props) =>{
    // States
    const [open, setOpen] = useState(false);

    // Abre el menu de compartir
    const toggleOpen = () =>{
        setOpen(!open);
    }
    
    return (
        <div className='e'>
            <button class={`dots ${open ? "open" : ""}`} onClick={toggleOpen}>
                <FontAwesomeIcon icon={faEllipsisV} size='2x' style={{width: '24px', height: '24px'}} className="dot"/>
                <FontAwesomeIcon icon={faTimes} size='2x' style={{width: '24px', height: '24px'}} className="close"/>
            </button>
        </div>
    );
};

export default MenuButton;