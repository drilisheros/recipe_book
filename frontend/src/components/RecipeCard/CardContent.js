import React, {useState} from 'react'

import './RecipeCard.css'

const CardContent = (props) =>{
    return (
        <div className='CardContent'>
            <img src={require("./hotdog.jpg")} alt="HotDog"></img>
            <p className="CardDate">Un pedazo de pan, una salchicha. Listo</p>
        </div>
    );
};

export default CardContent;