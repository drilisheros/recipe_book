import React, {useState} from 'react'

import CardHeader from './CardHeader'
import CardContent from './CardContent'
import ShareButton from '../ShareButton/ShareButton'

import './RecipeCard.css'

const RecipeCard = (props) =>{

    // Hooks de estado 
    const [count, setCount] = useState(0);

    return (
        <div className='RecipeCard'>
            <CardHeader 
                title='HotDog'
                date='7/21/2020'
            />
            <CardContent
                img=''
                desc=''
            />
            <ShareButton
            />
        </div>
    );
};

export default RecipeCard;