import React, {useState} from 'react'

import MenuButton from '../MenuButton/MenuButton'
import './RecipeCard.css'


const CardHeader = (props) =>{
    return (
        <div className='gridWrapper'>
            <div className="CardHeader">
                <p className="CardTitle">{props.title}</p>
                <p className="CardDate">{props.date}</p>
            </div>

            <div className ><MenuButton/></div>
        </div>
    );
};

export default CardHeader;