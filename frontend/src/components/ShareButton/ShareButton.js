import React, {useState} from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShareAlt, faTimes } from '@fortawesome/free-solid-svg-icons'
import { faInstagram, faFacebook, faTwitter } from '@fortawesome/free-brands-svg-icons'
import './ShareButton.css'

const CardButton = (props) =>{

    // States
    const [open, setOpen] = useState(false);

    // Abre el menu de compartir
    const toggleOpen = () =>{
        setOpen(!open);
    }

    return (
        <div className='content'>
            <button class={`shareButton main ${open ? "open" : ""}`} onClick={toggleOpen}>
                <FontAwesomeIcon icon={faShareAlt} size='2x'style={{width: '24px', height: '24px'}} className="share"/>
                <FontAwesomeIcon icon={faTimes} size='2x'style={{width: '24px', height: '24px'}} className="close"/>
            </button>
            <button class={`shareButton fb ${open ? "open" : ""}`}>
                <FontAwesomeIcon icon={faFacebook} size='2x'style={{width: '24px', height: '24px'}}/>
                </button>
            <button class={`shareButton tw ${open ? "open" : ""}`}>
                <FontAwesomeIcon icon={faTwitter} size='2x'style={{width: '24px', height: '24px'}}/>
                </button>
            <button class={`shareButton ig ${open ? "open" : ""}`}>
                <FontAwesomeIcon icon={faInstagram} size='2x'style={{width: '24px', height: '24px'}}/>
                </button>

        </div>
    );
};

export default CardButton;