import React from 'react';
import RecipeCard from '../components/RecipeCard/RecipeCard'

import './App.css';

function App() {
    return (
        <div className="App">
            <RecipeCard/>
        </div>
    );
}

export default App;
